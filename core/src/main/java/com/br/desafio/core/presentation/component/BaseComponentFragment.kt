package com.br.desafio.core.presentation.component

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

abstract class BaseComponentFragment : Fragment() {

    private val navController by lazy {
        findNavController()
    }

    fun navigate(navDirection: NavDirections) {
        navController.navigate(navDirection)
    }
}