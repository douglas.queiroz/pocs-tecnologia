package com.br.desafio.core.domain.navigator

interface IContrato {

    fun formataPath(): String
}