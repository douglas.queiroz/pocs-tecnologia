package com.br.desafio.core.domain.navigator

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri

private const val PREFIX: String = "desafio://"

class DesafioNavigator {

    companion object {
        fun navegacaoSemVolta(context: Context, contrato: IContrato) {
            context.startActivity(Intent(Intent.ACTION_VIEW).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                data = Uri.parse(PREFIX + contrato.formataPath())
            })

            finalizaNavegacao(context)
        }

        private fun finalizaNavegacao(context: Context) {
            when (context) {
                is Activity -> {
                    context.finish()
                }
            }
        }
    }
}