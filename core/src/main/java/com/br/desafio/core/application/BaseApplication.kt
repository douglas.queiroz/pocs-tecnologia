package com.br.desafio.core.application

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore

open class BaseApplication : Application() {
    val dataStore: DataStore<Preferences> by preferencesDataStore(name = "desafio-store")
}