package com.br.desafio.home

import androidx.room.Room
import androidx.room.migration.Migration
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.br.desafio.home.data.database.DesafioDatabase
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class MigrationTest {
    @Rule
    @JvmField
    val helper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        DesafioDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    @Throws(IOException::class)
    fun migrate1to2() {
        createDB(VERSION_ROOM_TEST)
        val appDb: DesafioDatabase =
            executeMigration(MIGRATION_1_2)
        simulateAcessRoom(appDb)
        assertTest()
    }

    private fun assertTest() {
        val testeString = ""
        Assert.assertNotNull(testeString)
    }

    @Throws(IOException::class)
    private fun createDB(versionInicialDB: Int): SupportSQLiteDatabase {
        val db: SupportSQLiteDatabase = helper.createDatabase(
            TEST_DB,
            versionInicialDB
        )
        db.close()
        return db
    }

    private fun executeMigration(vararg mirgration: Migration): DesafioDatabase {
        return Room.databaseBuilder(
            InstrumentationRegistry.getInstrumentation().targetContext,
            DesafioDatabase::class.java,
            TEST_DB
        )
            .addMigrations(*mirgration)
            .build()
    }

    private fun simulateAcessRoom(appDb: DesafioDatabase) {
        appDb.openHelper.writableDatabase
        appDb.close()
    }

    companion object {
        private const val TEST_DB = "desafio-migration-test"
        private const val VERSION_ROOM_TEST = 1
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `Card` add column teste TEXT");
            }
        }
    }
}