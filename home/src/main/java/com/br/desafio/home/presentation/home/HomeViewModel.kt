package com.br.desafio.home.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.home.domain.model.entity.Card
import com.br.desafio.home.domain.usercase.HomeUserCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val homeCase: HomeUserCase) : ViewModel() {

    private val _listCard = MutableLiveData<BaseResult<List<Card>>>()
    val listCard: LiveData<BaseResult<List<Card>>> = _listCard


    fun buscaCards() {
        viewModelScope.launch(Dispatchers.IO) {
            _listCard.postValue(homeCase.buscaCards())
        }
    }
}