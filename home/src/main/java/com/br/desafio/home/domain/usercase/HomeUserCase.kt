package com.br.desafio.home.domain.usercase

import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.home.R
import com.br.desafio.home.domain.model.entity.Card
import com.br.desafio.home.domain.irepository.IHomeRepository

class HomeUserCase(private val homeRepository: IHomeRepository) {

    suspend fun buscaCards(): BaseResult<List<Card>> {
        val cards = homeRepository.buscaCards()

        return if (cards.isNotEmpty()) {
            BaseResult.Success(cards.subList(0, 50))
        } else {
            BaseResult.Error(R.string.home_sem_resultado)
        }
    }
}