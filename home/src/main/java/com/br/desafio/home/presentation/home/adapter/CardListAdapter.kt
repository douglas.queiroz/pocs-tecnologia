package com.br.desafio.home.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.br.desafio.home.R
import com.br.desafio.home.domain.model.entity.Card

class CardListAdapter(
    private val listCard: List<Card>,
    private val clickCard: (card: Card) -> Unit
) :
    RecyclerView.Adapter<CardListItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardListItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.home_list_item_card, parent, false)

        return CardListItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardListItemViewHolder, position: Int) {
        val card = listCard[position]
        holder.bind(card)

        holder.itemView.setOnClickListener {
            clickCard.invoke(card)
        }
    }

    override fun getItemCount(): Int = listCard.size
}

