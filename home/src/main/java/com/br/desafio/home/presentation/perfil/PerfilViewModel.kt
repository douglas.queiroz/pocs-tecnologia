package com.br.desafio.home.presentation.perfil

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.desafio.home.domain.irepository.IPerfilRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PerfilViewModel(private val perfilRepository: IPerfilRepository) : ViewModel() {

    private val _sair = MutableLiveData<Boolean>()

    val sair: LiveData<Boolean> = _sair

    fun sair() {
        viewModelScope.launch(Dispatchers.IO) {
            perfilRepository.sair()
            _sair.postValue(true)
        }
    }

}