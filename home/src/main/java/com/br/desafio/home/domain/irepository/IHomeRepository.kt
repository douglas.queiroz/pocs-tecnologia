package com.br.desafio.home.domain.irepository

import com.br.desafio.home.domain.model.entity.Card

interface IHomeRepository {

    suspend fun buscaCards(): List<Card>
}