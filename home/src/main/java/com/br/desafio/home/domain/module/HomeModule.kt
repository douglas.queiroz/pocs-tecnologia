package com.br.desafio.home.domain.module

import com.br.desafio.core.data.request.RequestServiceApi
import com.br.desafio.home.BuildConfig
import com.br.desafio.home.data.database.DesafioDatabase
import com.br.desafio.home.data.request.CardApi
import com.br.desafio.home.presentation.home.HomeViewModel
import com.br.desafio.home.domain.usercase.HomeUserCase
import com.br.desafio.home.data.repository.HomeRepository
import com.br.desafio.home.presentation.perfil.PerfilViewModel
import com.br.desafio.home.data.repository.PerfilRepository
import com.br.desafio.home.domain.irepository.IHomeRepository
import com.br.desafio.home.domain.irepository.IPerfilRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val singleModuleHome = module {
    single {
        RequestServiceApi.getInstanceRetrofit(BuildConfig.URL_API).create(CardApi::class.java)
    }
}

val roomModuleHome = module {
    factory { DesafioDatabase.getIntanceDatabase(androidApplication()) }
    factory { get<DesafioDatabase>().cardDAO }
}

val casesModuleHome = module {
    factory { HomeUserCase(get()) }
}

val repositoryModuleHome = module {
    factory<IHomeRepository> { HomeRepository(get(), get()) }
    factory<IPerfilRepository> { PerfilRepository(get()) }
}

val viewModelModuleHome = module {
    viewModel { HomeViewModel(get()) }
    viewModel { PerfilViewModel(get()) }
}