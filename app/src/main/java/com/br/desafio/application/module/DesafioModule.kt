package com.br.desafio.application.module

import com.br.desafio.application.DesafioApplication
import com.br.desafio.core.data.datastore.DataStoreDesafio
import com.br.desafio.presentation.login.LoginViewModel
import com.br.desafio.data.source.LoginDataSource
import com.br.desafio.data.repository.LoginRepository
import com.br.desafio.domain.irepository.ILoginRepository
import com.br.desafio.domain.usercase.LoginUserCase
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val singleModule = module {
    single { (androidApplication() as DesafioApplication).dataStore }
    single { DataStoreDesafio(get()) }
    single { GsonBuilder().create() }
}

val serviceModule = module {
    factory { LoginDataSource() }
}

val repositoryModule = module {
    factory<ILoginRepository> { LoginRepository(get(), get(), get()) }
}

val userCaseModule = module {
    factory { LoginUserCase(get()) }
}

val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
}