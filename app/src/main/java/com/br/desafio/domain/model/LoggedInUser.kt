package com.br.desafio.domain.model

data class LoggedInUser(
    val userId: String,
    val displayName: String
)