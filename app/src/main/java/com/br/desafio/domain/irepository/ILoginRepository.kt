package com.br.desafio.domain.irepository

import com.br.desafio.domain.model.LoggedInUser

interface ILoginRepository {

    suspend fun verificaUsuarioLogado(): LoggedInUser?

    suspend fun login(username: String, password: String): LoggedInUser?
}