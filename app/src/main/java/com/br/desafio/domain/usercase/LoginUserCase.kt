package com.br.desafio.domain.usercase

import android.util.Patterns
import com.br.desafio.R
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.domain.irepository.ILoginRepository
import com.br.desafio.domain.model.LoggedInUser
import com.br.desafio.domain.model.LoginFormState

class LoginUserCase(private val loginRepository: ILoginRepository) {

    suspend fun verificaUsuarioLogado(): BaseResult<LoggedInUser> {
        return loginRepository.verificaUsuarioLogado()?.let {
            BaseResult.Success(it)
        } ?: run {
            BaseResult.Error(R.string.login_failed)
        }
    }

    suspend fun login(username: String, password: String): BaseResult<LoggedInUser> {
        return loginRepository.login(username, password)?.let {
            BaseResult.Success(it)
        } ?: run {
            BaseResult.Error(R.string.login_failed)
        }
    }

    fun loginDataChanged(username: String, password: String): LoginFormState {
        return if (!isUserNameValid(username)) {
            LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            LoginFormState(passwordError = R.string.invalid_password)
        } else {
            LoginFormState(isDataValid = true)
        }
    }

    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            false
        }
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }


}