package com.br.desafio.data.repository

import com.br.desafio.core.data.datastore.DataStoreDesafio
import com.br.desafio.core.data.datastore.PreferenceKey
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.data.source.LoginDataSource
import com.br.desafio.domain.irepository.ILoginRepository
import com.br.desafio.domain.model.LoggedInUser
import com.google.gson.Gson

class LoginRepository(
    private val dataSource: LoginDataSource,
    private val dataStoreDesafio: DataStoreDesafio,
    private val gson: Gson
) : ILoginRepository {

    override suspend fun verificaUsuarioLogado(): LoggedInUser? {
        return gson.fromJson(
            dataStoreDesafio.buscaValor(PreferenceKey.USUARIO_LOGADO),
            LoggedInUser::class.java
        )
    }

    override suspend fun login(username: String, password: String): LoggedInUser? {
        val user = dataSource.loginFake(username, password)

        user?.let {
            dataStoreDesafio.armazenaValor(
                PreferenceKey.USUARIO_LOGADO,
                gson.toJson(it)
            )
        }

        return user
    }
}